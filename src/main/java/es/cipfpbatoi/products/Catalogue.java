package es.cipfpbatoi.products;

import es.cipfpbatoi.exceptions.InvalidProductTypeException;
import es.cipfpbatoi.exceptions.NotFoundException;
import es.cipfpbatoi.products.type.*;
import es.cipfpbatoi.views.MenuCardViewList;

import java.util.ArrayList;

public class Catalogue {

    private ArrayList<Product> productList;

    public Catalogue() {
        this.productList = new ArrayList<>();
        setDefaultSandwichList();
        setDefaultDrinkList();
        setDefaultStarterList();
        setDefaultDesertList();
    }

    public Catalogue(ArrayList<Product> products) {
        this.productList = products;
    }

    public void add(Product product) {
        productList.add(product);
    }

    public void listAll(Class type) {
        ArrayList<Product> listado = new ArrayList<>();
        for (Product current: productList) {
            if (current.getClass() == type) {
                listado.add(current);
            }
        }
        MenuCardViewList menuCardView = new MenuCardViewList(listado);
        System.out.println(menuCardView);
    }

    public Product find(String cod) {
        for (Product product: productList) {
            if (product.getCod().equals(cod)){
                return product;
            }
        }
        return null;
    }

    public Product getProduct(String cod) throws NotFoundException {
        Product product = find(cod);
        if (product != null) {
           return product;
        }
        throw new NotFoundException(String.format("El artículo con codigo %s no existe", cod));
    }

    public static String getCodePrefix(Class classType) {
        if (classType == Desert.class) {
            return "p";
        } else if (classType == Drink.class) {
            return "b";
        } else if (classType == Sandwich.class) {
            return "m";
        } else if (classType == Starter.class) {
            return "e";
        } else {
            assert false;
        }
        throw new InvalidProductTypeException(classType);
    }

    private void setDefaultSandwichList() {
        final String productCodePrefix = Catalogue.getCodePrefix(Sandwich.class);
        productList.add(new Sandwich(productCodePrefix + 1, "lechuga, tomate y mayonesa"));
        productList.add(new Sandwich(productCodePrefix + 2, "HUEVO DURO lechuga, tomate y mayonesa"));
        productList.add(new Sandwich(productCodePrefix + 3, "VEGETAL CON QUESO lechuga, tomate y queso"));
        productList.add(new Sandwich(productCodePrefix + 4, "Burger, bacon ahumado, cebolla crujiente y alioli"));
        productList.add(new Sandwich(productCodePrefix + 5, "Pollo, bacon ahumado y salsa brava"));
        productList.add(new Sandwich(productCodePrefix + 6, "Pollo kebab, cebolla, pimiento verde y mayonesa"));
        productList.add(new Sandwich(productCodePrefix + 7, "CUATRO QUESOS: Queso ibérico, queso brie, queso de cabra y crema de queso"));
        productList.add(new Sandwich(productCodePrefix + 8, "CAPRESE: Jamón gran reserva, queso mozzarella, tomate y pesto"));
        productList.add(new Sandwich(productCodePrefix + 9, "Pulled pork y guacamole"));
        productList.add(new Sandwich(productCodePrefix + 10, "PULLED PORK y queso brie"));
        productList.add(new Sandwich(productCodePrefix + 11, "FILETE RUSO, cebolla caramelizada y salsa de queso cheddar"));
        productList.add(new Sandwich(productCodePrefix + 12, "SALMÓN AHUMADO y crema de queso"));
        productList.add(new Sandwich(productCodePrefix + 13, "CARNE MECHADA DESHILACHADA y cebolla crujiente"));
        productList.add(new Sandwich(productCodePrefix + 14, "JAMÓN GRAN RESERVA, tomate y aceite de oliva virgen extra"));
        productList.add(new Sandwich(productCodePrefix + 15, "CARRILLERA AL VINO TINTO y queso ibérico"));
        productList.add(new Sandwich(productCodePrefix + 16, "QUESO IBÉRICO, tortilla de patatas y mayonesa"));
        productList.add(new Sandwich(productCodePrefix + 17, "ALBÓNDIGAS y salsa BBQ"));
        productList.add(new Sandwich(productCodePrefix + 18, "Pollo, cebolla caramelizada y mayonesa trufada"));
        Product product = new Sandwich(productCodePrefix + 19, "CHISTORRA, bacon ahumado y salsa brava");
        product.setDiscount(0.2f);
        productList.add(product);
        productList.add(new Sandwich(productCodePrefix + 20, "Tortilla de patatas"));

    }

    private void setDefaultDrinkList() {
        final String productCodePrefix = Catalogue.getCodePrefix(Drink.class);
        productList.add(new Drink(productCodePrefix + 1, "Coca-Cola 33cl"));
        productList.add(new Drink(productCodePrefix + 2, "Coca-Cola 1l", false, Drink.Size.BIG, 2, 0.25f));
        productList.add(new Drink(productCodePrefix + 3, "Agua"));
        productList.add(new Drink(productCodePrefix + 4, "Fanta Limón"));
        productList.add(new Drink(productCodePrefix + 5, "Fanta Naranja"));
        productList.add(new Drink(productCodePrefix + 6, "Cerveza bote 33cl"));
        productList.add(new Drink(productCodePrefix + 7, "Caña Cerveza", Drink.Size.NORMAL));
        productList.add(new Drink(productCodePrefix + 8, "Jarra Cerveza", false, Drink.Size.BIG, 6, 0.1f));
        productList.add(new Drink(productCodePrefix + 9, "Cerveza Jarra Infinita (Promoción)", true, Drink.Size.BIG, 10, 0.5f));
    }

    private void setDefaultDesertList() {
        final String productCodePrefix = Catalogue.getCodePrefix(Desert.class);
        productList.add(new Desert(productCodePrefix + 1, "Pastel de Queso",
                Desert.Characteristic.CELIAC_SUITABLE));
        productList.add(new Desert(productCodePrefix + 2, "Pastel Chocolate"));
        productList.add(new Desert(productCodePrefix + 3, "Helado Chocolate",
                Desert.Characteristic.DIABETIC_SUITABLE));
        productList.add(new Desert(productCodePrefix + 4, "Helado Vainilla",
                Desert.Characteristic.CELIAC_SUITABLE));
        productList.add(new Desert(productCodePrefix + 5, "Helado Limón",
                Desert.Characteristic.CELIAC_SUITABLE, Desert.Characteristic.DIABETIC_SUITABLE));
        productList.add(new Desert(productCodePrefix + 6, "Helado Fresa",
                Desert.Characteristic.CELIAC_SUITABLE, Desert.Characteristic.DIABETIC_SUITABLE));
    }

    private void setDefaultStarterList() {
        final String productCodePrefix = Catalogue.getCodePrefix(Starter.class);
        productList.add(new Starter(productCodePrefix + 1, "Patatas 4 Quesos", 2, 1.38f));
        productList.add(new Starter(productCodePrefix + 2, "Bolas de pollo"));
        productList.add(new Starter(productCodePrefix + 3, "Aceitunas"));
        productList.add(new Starter(productCodePrefix + 4, "Nachos"));
        productList.add(new Starter(productCodePrefix + 5, "Ensalada de la casa"));
        productList.add(new Starter(productCodePrefix + 6, "Bolas de queso"));
        productList.add(new Starter(productCodePrefix + 7, "Alitas de pollo (mini)",2, 2));
        productList.add(new Starter(productCodePrefix + 8, "Alitas de pollo (super)",4, 4));
        productList.add(new Starter(productCodePrefix + 9, "Patatas fritas", 2 ,1));
        productList.add(new Starter(productCodePrefix + 10, "Patatas fritas (maxi)", 4, 2));
    }


}
