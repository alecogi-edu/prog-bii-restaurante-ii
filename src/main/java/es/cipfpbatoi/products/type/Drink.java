package es.cipfpbatoi.products.type;

public class Drink extends Product {

    public enum Size {BIG, NORMAL, SMALL}

    private boolean refillable;

    private Size size;

    public Drink(String cod, String name , boolean refillable, Size size, float prize, float discount) {
        super(cod, name, prize, discount);
        this.refillable = refillable;
        this.size = size;
    }

    public Drink(String cod, String name, Size size) {
        this(cod, name, false, size, 1.38f, 0);
    }

    public Drink(String cod, String name) {
        this(cod, name, Size.SMALL);
    }



    public Drink(String cod) {
        super(cod);
    }

    public boolean isRefillable() {
        return refillable;
    }

    public Size getSize() {
        return size;
    }
}
