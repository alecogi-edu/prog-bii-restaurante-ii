package es.cipfpbatoi.products.type;

public class Sandwich extends Product {

    public Sandwich(String cod, String name) {
        super(cod, name, 1.38f, 0);
    }

}
