package es.cipfpbatoi.products.type;

public class Starter extends Product {

    private int rations;

    private final static int PRIZE_PER_RATION = 1;

    public Starter(String cod, String name, int rations, float prize) {
        super(cod, name, prize, 0);
        this.rations = rations;
    }

    public Starter(String cod, String name) {
        this(cod, name, 1, 1.38f);
    }

    public Starter(String cod) {
        super(cod);
    }

    public int getRations() {
        return rations;
    }

    @Override
    public float getPrize() {
        return super.getPrize() + rations * PRIZE_PER_RATION;
    }
}
