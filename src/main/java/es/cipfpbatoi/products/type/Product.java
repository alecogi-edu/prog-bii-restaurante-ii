package es.cipfpbatoi.products.type;

public abstract class Product {

    public static final float GENERAL_VAT = 0.1f;
    public static final float GENERAL_PRIZE = 1.25f;
    private String cod;

    private String name;

    private float prize;

    private float discount;

    private float vat;

    public Product(String cod, String name, float prize, float discount, float vat) {
        this.cod = cod;
        this.name = name;
        this.prize = prize;
        this.discount = discount;
        this.vat = vat;
    }

    public Product(String cod, String name, float prize, float discount) {
        this(cod, name, prize, discount, GENERAL_VAT);
    }

    public Product(String cod, String name, float prize) {
        this(cod, name, prize, 0);
    }

    public Product(String cod, String name) {
        this(cod, name, GENERAL_PRIZE);
    }

    public Product(String cod) {
        this(cod,null);
    }

    public String getCod() {
        return this.cod;
    }

    public float getPrize() {
        return prize * (1 + vat) - (prize * discount);
    }

    public float getPrizeWithoutDiscount() {
        return prize * (1 + vat);
    }


    public String getName() {
        return name;
    }

    public void setDiscount(float discount) {
        this.discount = discount;
    }

    public float getPercentageDiscount() {
        return discount * 100;
    }

}
