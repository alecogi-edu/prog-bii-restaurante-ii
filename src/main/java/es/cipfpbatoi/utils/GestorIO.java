package es.cipfpbatoi.utils;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.InputMismatchException;
import java.util.Scanner;

public class GestorIO {

    private static final String SPANISH_DATE_FORMAT = AnsiColor.colorize(AnsiColor.HIGH_INTENSITY,  " (dd/mm/yyyy): ");


    private static Scanner scanner;

    static {
        scanner = new Scanner(System.in);
    }

    public static int obtenerEntero(String mensaje) {
        do {
            System.out.print(mensaje);
            if (!scanner.hasNextInt()) {
                AnsiColor.errorOutput("Error! Debe introducir un entero");
                scanner.nextLine();
            } else {
                int number = scanner.nextInt();
                scanner.nextLine();
                return number;
            }
        } while (true);
    }

    public static String obtenerCadena(String mensaje) {
        String cadena;
        System.out.print(mensaje);
        do {
            cadena = scanner.nextLine().trim();
        } while (cadena.length() == 0);
        return cadena;

    }

    public static boolean confirmar(String mensaje) {
        String confirmacion;
        do {
            confirmacion = obtenerCadena(mensaje + " [S/N]: ").toUpperCase();
            if (Validator.isValidConfirmation(confirmacion)) {
                return confirmacion.charAt(0) == 'S';
            }
            AnsiColor.errorOutput("Debe introducir un valor válido");
        } while (true);

    }

    public static LocalDate obtenerFecha(String mensaje) {
        do {
            try {
                String fecha = obtenerCadena(mensaje + SPANISH_DATE_FORMAT);
                validateDateFormat(fecha);
                return LocalDate.parse(fecha, DateTimeFormatter.ofPattern("dd/MM/yyyy"));
            } catch (InputMismatchException e) {
                AnsiColor.errorOutput(e.getMessage());
            }
        } while (true);
    }

    private static void validateDateFormat(String fecha) {
        if (!Validator.isValidateDate(fecha)) {
           throw new InputMismatchException("El formato de la fecha introducida es incorrecta" + SPANISH_DATE_FORMAT);
        }
    }

}