package es.cipfpbatoi;

import es.cipfpbatoi.menu.MenuAppNew;
import es.cipfpbatoi.menu.options.ExitOption;
import es.cipfpbatoi.menu.options.NewOrderOption;
import es.cipfpbatoi.menu.options.core.PrepareOrderOption;
import es.cipfpbatoi.menu.options.SearchHistoricOrderOption;
import es.cipfpbatoi.menu.options.SearchPendingOrderOption;
import es.cipfpbatoi.products.Catalogue;
import es.cipfpbatoi.restaurante.Restaurant;
import es.cipfpbatoi.utils.AnsiColor;

public class App {

    public static void main(String[] args) {

        Catalogue catalogue = new Catalogue();

        //Creamos el restaurante con la carta
        Restaurant restaurant = new Restaurant(catalogue);

        //Creamos un menu para que gestiones nuestro restaurante
        String tituloMenu = getTituloMenu();
        MenuAppNew restaurantMenu = new MenuAppNew(tituloMenu);
        restaurantMenu.anyadir(new NewOrderOption(restaurant));
        restaurantMenu.anyadir(new PrepareOrderOption(restaurant));
        restaurantMenu.anyadir(new SearchHistoricOrderOption(restaurant));
        restaurantMenu.anyadir(new SearchPendingOrderOption(restaurant));
        restaurantMenu.anyadir(new ExitOption());
        restaurantMenu.ejecutar();

    }

    private static String getTituloMenu() {
       return "==============================================\n" +
                AnsiColor.colorize(AnsiColor.BLUE, "=== Bienvenido al bar de los 20 Montaditos ===\n") +
                "==============================================";
    }

}
