package es.cipfpbatoi.orders;

import es.cipfpbatoi.exceptions.EmptyQueueException;
import es.cipfpbatoi.exceptions.FullQueueException;

import java.util.ArrayList;

public class PendingOrdersQueue {

    private final static int MAX_QUEUE_ELEMENTS = 100;

    private ArrayList<Order> pendingOrders;

    public PendingOrdersQueue() {
        pendingOrders = new ArrayList<>();
    }

    public void add(Order order) {
        assertQueueIsNotFull();
        pendingOrders.add(order);
    }

    public Order next() {
        if (pendingOrders.size() > 0) {
            Order nextOrder = pendingOrders.get(0);
            pendingOrders.remove(nextOrder);
            return nextOrder;
        }
        throw new EmptyQueueException();
    }

    public boolean isFull(){
        return (pendingOrders.size() == MAX_QUEUE_ELEMENTS);
    }

    public boolean isEmpty(){
        return (pendingOrders.size() == 0);
    }

    private void assertQueueIsNotFull(){
        if (isFull()) {
            throw new FullQueueException();
        }
    }

    public void reallocate(Order currentOrder) {
        pendingOrders.add(0, currentOrder);
    }

    public ArrayList<Order> findAll() {
        return pendingOrders;
    }

    public int size() {
        return pendingOrders.size();
    }
}
