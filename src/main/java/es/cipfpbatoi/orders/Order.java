package es.cipfpbatoi.orders;

import es.cipfpbatoi.exceptions.ServedOrderException;
import es.cipfpbatoi.products.type.Product;

import java.time.LocalDateTime;
import java.util.ArrayList;

public class Order {

    private String code;

    private String clientName;

    private LocalDateTime createdOn;

    private boolean served;

    private ArrayList<Product> products;

    public Order(String code, String clientName, LocalDateTime createdOn) {

        this.clientName = clientName;
        this.createdOn = createdOn;
        this.code = code;
        served = false;
        this.products = new ArrayList<>();

    }

    public void addNewProduct(Product product) {

        products.add(product);

    }

    public String getClientName() {

        return clientName;

    }

    public LocalDateTime getCreatedOn() {

        return createdOn;

    }

    public boolean isServed() {

        return served;

    }

    public String getCode() {

        return code;

    }

    public void setServed() throws ServedOrderException{
         if (isServed()){
             throw new ServedOrderException();
         }
         served = true;
    }

    public ArrayList<Product> getProducts() {

        return products;

    }

    public boolean hasProducts() {

        return products.size() > 0;

    }

    public double getOrderPrize() {

        double totalPrize = 0;
        for (Product product : products) {
            totalPrize += product.getPrize();
        }
        return totalPrize;

    }

}
