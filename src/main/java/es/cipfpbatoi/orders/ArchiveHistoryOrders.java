package es.cipfpbatoi.orders;

import es.cipfpbatoi.exceptions.NotFoundException;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;

public class ArchiveHistoryOrders {

    private HashMap<String, Order> orderList;

    public ArchiveHistoryOrders() {
        orderList = new HashMap<>();
    }

    public void add(Order order) {
        orderList.put(order.getCode(), order);
    }

    public int size() {
        return orderList.size();
    }

    public ArrayList<Order> findAll() {
        return new ArrayList<>(orderList.values());
    }

    public ArrayList<Order> findByDate(LocalDate date) {
        ArrayList<Order> listadoFiltrado = new ArrayList<>();
        for (Order order: findAll()) {
            if (order.getCreatedOn().toLocalDate().equals(date)) {
                listadoFiltrado.add(order);
            }
        }
        return listadoFiltrado;
    }

    public Order find(String orderCode) {
        if (orderList.containsKey(orderCode)) {
            return orderList.get(orderCode);
        }
        return null;
    }

    public Order getByCod(String orderCode) throws NotFoundException {
        if (orderList.containsKey(orderCode)) {
            return orderList.get(orderCode);
        }
        throw new NotFoundException(String.format("El pedido con codigo %s no existe", orderCode));
    }

    public boolean isEmpty (){
        return orderList.size() == 0;
    }

}
