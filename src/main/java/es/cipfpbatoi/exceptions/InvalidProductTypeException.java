package es.cipfpbatoi.exceptions;

public class InvalidProductTypeException extends RuntimeException {

    public InvalidProductTypeException(Class tipo) {
        super("El tipo de producto %s especificado es incorrecto " + tipo.getSimpleName());
    }

}
