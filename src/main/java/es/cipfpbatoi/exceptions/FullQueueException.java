package es.cipfpbatoi.exceptions;

public class FullQueueException extends RuntimeException {

    public FullQueueException() {
        super("Se ha alzanzado el número máximo de pedidos pendientes de servir");
    }

}
