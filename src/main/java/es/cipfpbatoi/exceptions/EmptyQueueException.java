package es.cipfpbatoi.exceptions;

public class EmptyQueueException extends RuntimeException {

    public EmptyQueueException() {
        super("La cola de pedidos está vacía");
    }

}
