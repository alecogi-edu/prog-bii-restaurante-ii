package es.cipfpbatoi.exceptions;

public class ServedOrderException extends RuntimeException {

    public ServedOrderException() {
        super("Este pedido ya ha sido servido");
    }

}
