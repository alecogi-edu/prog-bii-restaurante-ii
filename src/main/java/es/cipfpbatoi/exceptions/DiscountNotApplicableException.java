package es.cipfpbatoi.exceptions;

public class DiscountNotApplicableException extends RuntimeException {

    public DiscountNotApplicableException(String cod) {
        super("En el producto con cod " + cod + " el descuento no es aplicable");
    }

}
