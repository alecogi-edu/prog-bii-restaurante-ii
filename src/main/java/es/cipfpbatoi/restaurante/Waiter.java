package es.cipfpbatoi.restaurante;

import es.cipfpbatoi.orders.Order;
import es.cipfpbatoi.utils.AnsiColor;
import es.cipfpbatoi.utils.GestorIO;
import es.cipfpbatoi.utils.Validator;
import es.cipfpbatoi.exceptions.NotFoundException;
import es.cipfpbatoi.products.Catalogue;
import es.cipfpbatoi.products.type.*;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.InputMismatchException;

public class Waiter {

    private final DateTimeFormatter dateFormatterES = DateTimeFormatter.ofPattern("dd/MM/yyyy");

    private final Catalogue catalogue;

    private Order order;

    public Waiter(Catalogue catalogue) {
        this.catalogue = catalogue;
    }

    /**
     * Crea un nuevo Pedido con el identificador @code
     *
     * @param code
     * @return Order
     */
    public Order attend(String code) {
        String clientName = getClientName();
        LocalDateTime orderDateTime =  getOrderDateTime();
        order = new Order(code, clientName, orderDateTime);
        askForProducts();
        return (order.hasProducts()) ? order : null;
    }

    /**
     * Preguntar al usuario por los productos que desea tomar
     */
    private void askForProducts() {
        System.out.println("Buenos dias, que deseas Tomar?");
        System.out.println("¿Qué desea beber?");
        selectProductsOfType(Drink.class);
        System.out.println("¿Desea tomar algo de entrante?");
        selectProductsOfType(Starter.class);
        System.out.println("¿Desea bocadillos desea tomar?");
        selectProductsOfType(Sandwich.class);
        System.out.println("¿Quieres algo de postre?");
        selectProductsOfType(Desert.class);
    }

    /**
     *  Selecciona productos del tipo @productClass y los añade al pedido
     *
     * @param productClass
     */
    private void selectProductsOfType(Class productClass){
        catalogue.listAll(productClass);
        do {
            try {
                Product product = askForNewProduct();
                if (product == null) {
                    break;
                }
                if (product.getClass() != productClass) {
                    if (!confirmSelection()) {
                        continue;
                    }
                }
                addNewProductToOrder(product);
            }catch (InputMismatchException | NotFoundException e) {
                AnsiColor.errorOutput(e.getMessage());
            }
        } while (true);
    }

    /**
     * Preguntamos al usuario que producto nuevo quiere añadir
     * @return Producto a añadir
     */
    private Product askForNewProduct() throws NotFoundException {
        String code = getProductCode();
        if (code == null) {
            return null;
        }
        return catalogue.getProduct(code);
    }

    /**
     * Obtenemos el código del producto a introducir / null si no quiere mas
     * @return codigo del producto a añadir
     */
    private String getProductCode() {
        String mensaje = String.format("Introduzca el código del producto que desea añadir %s \n",
                AnsiColor.colorize(AnsiColor.CYAN, "(0 - Finalizar)"));
        String productCode = GestorIO.obtenerCadena(mensaje);
        if (productCode.equalsIgnoreCase("0")){
            return null;
        }
        if(!Validator.isValidProductCode(productCode)) {
            throw new InputMismatchException("El código de producto introducido no es válido");
        }
        return productCode;
    }

    /**
     * Añade el producto seleccionado a la orden
     *
     * @param product
     */
    private void addNewProductToOrder(Product product) {
        order.addNewProduct(product);
        System.out.printf("%s - %s %s \n",
                AnsiColor.colorize(AnsiColor.GREEN, product.getCod()),
                product.getName(),
                AnsiColor.colorize(AnsiColor.GREEN,  "[Añadido]"));
    }

    /**
     * Obtiene el nombre del cliente que quiere realizar el pedido
     *
     * @return nombreCliente
     */
    private String getClientName() {
        return GestorIO.obtenerCadena("Introduzca su nombre: ");
    }

    private LocalDateTime getOrderDateTime() {
        LocalDateTime now = LocalDateTime.now();
        String mensaje = String.format("Fecha Actual: %s%n¿Quieres utilizarla?",
                AnsiColor.colorize(AnsiColor.BLUE, now.format(dateFormatterES)));
        boolean quieresUtilizarla = GestorIO.confirmar(mensaje);
        if (!quieresUtilizarla) {
            LocalDate manualDate = askForOrderDate();
            return LocalDateTime.of(manualDate, LocalTime.of(8,0,0));
        } else {
            return now;
        }
    }
    /**
     * Devuelve la fecha del pedido en formato dd/mm/yyyy
     *
     * @return
     */
    private LocalDate askForOrderDate() {
        do {
            try {
                return GestorIO.obtenerFecha("Introduzca la fecha del pedido");
            } catch (DateTimeParseException e) {
                AnsiColor.errorOutput(e.getMessage());
            }
        } while (true);
    }

    private boolean confirmSelection() {
        String message = AnsiColor.colorize(AnsiColor.GREEN,"El producto seleccionado no pertenece a la categoría, desea agregarlo ");
        return GestorIO.confirmar(message);
    }

}
