package es.cipfpbatoi.restaurante;
import es.cipfpbatoi.exceptions.NotFoundException;
import es.cipfpbatoi.orders.Order;
import es.cipfpbatoi.utils.AnsiColor;
import es.cipfpbatoi.utils.GestorIO;
import es.cipfpbatoi.utils.Validator;
import es.cipfpbatoi.orders.ArchiveHistoryOrders;
import es.cipfpbatoi.orders.PendingOrdersQueue;
import es.cipfpbatoi.products.Catalogue;
import es.cipfpbatoi.views.SingleOrderView;
import es.cipfpbatoi.views.OrderViewList;

import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.InputMismatchException;

public class Restaurant {

    private Waiter waiter;

    private ArchiveHistoryOrders archiveHistoryOrders;

    private PendingOrdersQueue pendingOrdersQueue;

    public Restaurant(Catalogue catalogue) {
        this.waiter = new Waiter(catalogue);
        this.pendingOrdersQueue = new PendingOrdersQueue();
        this.archiveHistoryOrders = new ArchiveHistoryOrders();
    }

    /**
     *  Registra un nuevo pedido en el restaurante
     */
    public void attendClient() {
        Order order =  waiter.attend(getNextOrderCode());
        if (order != null) {
            pendingOrdersQueue.add(order);
            AnsiColor.colorizeOutput(AnsiColor.GREEN, "Pedido registrado con éxito ");
        } else {
            AnsiColor.errorOutput("No se ha posido registrar el pedido. El cliente se lo está pensando");
        }
    }

    /**
     *  Permite marcar una orden como servida
     */
    public void prepareOrder() {
        if (pendingOrdersQueue.isEmpty()) {
            AnsiColor.errorOutput("No existen ordenes pendientes de servir");
            return;
        }
        Order currentOrder = pendingOrdersQueue.next();
        showOrder(currentOrder);
        boolean confirmacion = GestorIO.confirmar("Quieres marca la orden seleccionada como servida");
        if (confirmacion) {
            currentOrder.setServed();
            archiveHistoryOrders.add(currentOrder);
            AnsiColor.colorizeOutput(AnsiColor.BLUE, "Orden: " + currentOrder.getCode() + " servida");
        } else {
            pendingOrdersQueue.reallocate(currentOrder);
        }
    }

    /**
     * Buscar y visualizar una orden del historico de pedidos servidos
     */
    public void searchHistoricalOrder() {
        try {
            LocalDate filterDate = GestorIO.obtenerFecha("Introduzca una fecha para buscar");
            ArrayList<Order> filteredOrders = archiveHistoryOrders.findByDate(filterDate);
            showOrderList(filteredOrders);
            if (filteredOrders.size() > 0) {
                String orderCode = askForOrderCode();
                Order order = archiveHistoryOrders.getByCod(orderCode);
                showOrder(order);
            }
        } catch (NotFoundException | DateTimeParseException e) {
            AnsiColor.errorOutput(e.getMessage());
        }
    }

    /**
     * Buscar y visualizar una orden pendiente de servir/preparar
     */
    public void searchPendingOrder() {
        showOrderList(pendingOrdersQueue.findAll());
        try {
            if (pendingOrdersQueue.size() > 0) {
                String orderCode = askForOrderCode();
                Order order = archiveHistoryOrders.getByCod(orderCode);
                showOrder(order);
            }
        } catch (NotFoundException | InputMismatchException e) {
            AnsiColor.errorOutput(e.getMessage());
        }
    }

    private String askForOrderCode() {
        String orderCode = GestorIO.obtenerCadena("Introduzca el código de la orden que deseas Visualizar: ");
        if (!Validator.isValidOrderCode(orderCode)){
            throw new InputMismatchException("El codigo introducido no es válido");
        }
        return orderCode;
    }

    private void showOrder(Order order) {
        SingleOrderView orderView = new SingleOrderView(order);
        System.out.println(orderView);
    }

    private void showOrderList(ArrayList<Order> orderList) {
        OrderViewList orderViewList = new OrderViewList(orderList);
        System.out.println(orderViewList);
    }

    private String getNextOrderCode(){
        return "o" + (pendingOrdersQueue.size() + archiveHistoryOrders.size() + 1);
    }
}
