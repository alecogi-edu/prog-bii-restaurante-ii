package es.cipfpbatoi.menu.options;

import es.cipfpbatoi.menu.options.core.RestaurantOption;
import es.cipfpbatoi.restaurante.Restaurant;

public class SearchPendingOrderOption extends RestaurantOption {

    public SearchPendingOrderOption(Restaurant restaurant) {
        super("Buscar y visualizar pedidos (Pendientes)", restaurant);
    }

    @Override
    public void ejecutar() {
        restaurant.searchPendingOrder();
    }
}
