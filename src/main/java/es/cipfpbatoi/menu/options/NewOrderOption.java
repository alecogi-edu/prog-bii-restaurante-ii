package es.cipfpbatoi.menu.options;

import es.cipfpbatoi.menu.options.core.RestaurantOption;
import es.cipfpbatoi.restaurante.Restaurant;

public class NewOrderOption extends RestaurantOption {

    public NewOrderOption(Restaurant restaurant) {
        super("Crear Nuevo Pedido", restaurant);
    }

    @Override
    public void ejecutar() {
        restaurant.attendClient();
    }
}
