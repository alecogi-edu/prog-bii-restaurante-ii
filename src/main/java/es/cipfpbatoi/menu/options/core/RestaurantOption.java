package es.cipfpbatoi.menu.options.core;

import es.cipfpbatoi.restaurante.Restaurant;

public abstract class RestaurantOption extends Option {

    protected Restaurant restaurant;

    public RestaurantOption(String titulo, Restaurant restaurant) {
        super(titulo);
        this.restaurant = restaurant;
    }

}
