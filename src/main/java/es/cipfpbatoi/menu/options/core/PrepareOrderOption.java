package es.cipfpbatoi.menu.options.core;

import es.cipfpbatoi.restaurante.Restaurant;

public class PrepareOrderOption extends RestaurantOption {

    public PrepareOrderOption(Restaurant restaurant) {
        super("Preparar Pedido", restaurant);
    }

    @Override
    public void ejecutar() {
        restaurant.prepareOrder();
    }
}
