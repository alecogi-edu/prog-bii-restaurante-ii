package es.cipfpbatoi.menu.options;

import es.cipfpbatoi.menu.options.core.RestaurantOption;
import es.cipfpbatoi.restaurante.Restaurant;

public class SearchHistoricOrderOption extends RestaurantOption {

    public SearchHistoricOrderOption(Restaurant restaurant) {
        super("Buscar y visualizar pedidos (Historico)", restaurant);
    }

    @Override
    public void ejecutar() {
        restaurant.searchHistoricalOrder();
    }
}
