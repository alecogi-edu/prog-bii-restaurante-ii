package es.cipfpbatoi.menu.options;

import es.cipfpbatoi.menu.options.core.Option;
import es.cipfpbatoi.utils.AnsiColor;

public class ExitOption extends Option {

    public ExitOption() {
        super("Salir");
    }

    @Override
    public void ejecutar() {
        System.out.println("==============================================");
        AnsiColor.colorizeOutput(AnsiColor.BLUE, "=========== Esperamos verte pronto ===========");
        System.out.println("==============================================");
        this.setFinalizar(true);
    }
}
