package es.cipfpbatoi.menu;

import es.cipfpbatoi.menu.options.core.Option;
import es.cipfpbatoi.utils.AnsiColor;
import es.cipfpbatoi.utils.GestorIO;

import java.util.ArrayList;

public class MenuAppNew {

    private ArrayList<Option> opciones;

    private String tituloMenu;

    public MenuAppNew(String tituloMenu) {
        this.opciones = new ArrayList<>();
        this.tituloMenu = tituloMenu;
    }

    public void ejecutar(){
        Option opcion;
        do {
            mostrar();
            opcion = getOpcion();
            opcion.ejecutar();
        } while (!opcion.finalizar());
    }

    public void anyadir(Option opcion) {
        this.opciones.add(opcion);
    }

    private void mostrar() {
        System.out.print("\n" + tituloMenu + "\n");
        for (int i = 0; i < opciones.size() ; i++) {
            opciones.get(i).mostrar(i + 1);
        }
    }

    private Option getOpcion() {
        do {
            int opcion = GestorIO.obtenerEntero("\nSeleccione una opcion [1-" + opciones.size()+ "]: ");
            if (opcion >= 1 && opcion <= opciones.size()) {
                return this.opciones.get(opcion - 1);
            }
            AnsiColor.errorOutput("Error! La opcion seleccionada no existe");
        } while (true);
    }

}
